//
//  ViewController.swift
//  juego
//
//  Created by AlejandroPardina on 14/11/17.
//  Copyright © 2017 AlejandroPardina. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let gameModel = GameModel()
    
    @IBOutlet weak var objetivoLabel: UILabel!
    @IBOutlet weak var PuntajeLabel: UILabel!
    @IBOutlet weak var rondaLabel: UILabel!
    @IBOutlet weak var slider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setValores()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func jugarButton(_ sender: Any) {
        gameModel.jugar(valorIntento: Int(round(slider.value)))
        setValores()
        
    }
    
    @IBAction func reiniciarButton(_ sender: Any) {
        gameModel.reiniciar()
        setValores()
    }
    func setValores()
    {
        objetivoLabel.text = "Mueve cerrca de: \(gameModel.objetivo ?? 0)"
        PuntajeLabel.text = "Puntaje: \(gameModel.puntaje)"
        rondaLabel.text = "Ronda: \(gameModel.ronda)"
    }
}

